#include "audiogrid.h"


AudioGrid::AudioGrid(QString name, quint64 activation_time)
{
    this->_name = name;
    this->_timer = new ModifiedTimer(activation_time, 500);
    iterator = new QListIterator<MediaFile*>(_files);
    connect(_timer, SIGNAL(grid_enabled()), this, SLOT(play()));
    _timer->begin();
}

AudioGrid::~AudioGrid()
{
    delete _timer;
    _files.clear();
}

void AudioGrid::addMediaFromFile(QString path)
{
    qInfo() << "Adding file:" << path << "to grid:" << _name;
    if(QFile(path).exists()){
        _files.append(createMF(path));
        calculateDuration();
    } else {
        qWarning() << "File:" << path << "Doesn't exists";
    }
}

QList<MediaFile *> AudioGrid::getFiles() const
{
    return _files;
}

ModifiedTimer *AudioGrid::getTimer() const
{
    return _timer;
}

void AudioGrid::calculateDuration()
{
    _secDuration = 0;
    for(MediaFile* file : _files)
    {
        _secDuration += file->s_duration;
    }
}

void AudioGrid::saveToFile()
{
    if(!QDir(QDir::currentPath() + QDir::separator() + "grids").exists()){
        QDir::current().mkdir("grids");
    }
    QFile file(QDir::currentPath() + QDir::separator() + "grids" + QDir::separator() + _name + ".aag");
    file.open(QIODevice::WriteOnly);
    QTextStream out(&file);
    out << _name << '\n';
    out << _timer->getTime()  << '\n';
    for(MediaFile* f : _files){
        out << f->path << '\n';
    }
}

MediaFile* AudioGrid::createMF(QString path)
{
    MediaFile* media  = new MediaFile;
    media->path = path;
    media->name = path.split(QDir::separator()).last();
    return media;
}

AudioGrid  *AudioGrid::fromFile(QString path)
{
    qInfo() << "Opening the file:" << path;
    QFile file(path);
    if(file.open(QIODevice::ReadOnly)){
        QTextStream in(&file);
        QString name = in.readLine();
        quint64 activation_time = in.readLine().toLongLong();
        AudioGrid* grid = new AudioGrid(name, activation_time);
        while(!in.atEnd()){
            QString s = in.readLine(100000);
            if(s != QString('\n')){
                grid->addMediaFromFile(s);
            }
        }
        return grid;
    } else {
        qWarning() << "Can't open the file:" << path;
        return Q_NULLPTR;
    }
}

void AudioGrid::play()
{
    emit readyToPlay(this);
}
