#ifndef ARDUINOSERIALWIDGET_H
#define ARDUINOSERIALWIDGET_H

#include <QtWidgets>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QTextStream>
#include <QTime>
#include <QDateTime>
#include <global.h>

class ArduinoSerialWidget : public QWidget
{
    Q_OBJECT
private:
    QSerialPort* serial;
    QLCDNumber* schoolTime;
    QLCDNumber* realTime;
    QPushButton* amplifier;
    QLabel* deviceName;
    QTimer* connectionTimer;
    QTimer* timeUpdateTimer;
    QTimeEdit* dte;
    QPushButton* b_send_time;

    QFrame* generateLine();

    bool amplifierStatus = false;
    bool isConnected = false;

    void getTimeCorrection();
    QString readString();
    void writeString(QString data);
public:
    ArduinoSerialWidget();
    void setAmplifierEnabled(bool a_enabled);
public slots:
    void amplifierBPressed();
    void timeSendBPressed();
private slots:
    void tryConnect();
    void updateTime();
    void serialError(QSerialPort::SerialPortError error);
};

#endif // ARDUINOSERIALWIDGET_H
