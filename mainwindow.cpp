#include "mainwindow.h"

void MainWindow::loadGrids()
{
    QDir d = QDir::current().path() + QDir::separator() + QString("grids");
    QStringList filters;
    filters.append("*.aag");
    QList<QString> availableGrids = d.entryList(filters);
    for(QString name : availableGrids){
        name = d.path() + QDir::separator() + name;
        AudioGrid* newGrid = AudioGrid::fromFile(name);
        grids.append(newGrid);
        connect(newGrid, SIGNAL(readyToPlay(AudioGrid*)), this, SLOT(gridTimerTimeout(AudioGrid*)));
    }
    for(AudioGrid* grid : grids){
        gridsView->addItem(grid->getName());
    }
}

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
{
    gridInfo = new GridShowWidget;
    gridsView = new QComboBox;
    play = new QPushButton("Проиграть сетку");
    menuBar = new QMenuBar;
    lwg = new QListWidget;
    player = new AudioPlayer;
    asw = new ArduinoSerialWidget;
    server = new QTcpSocket(this);

    server->connectToHost("192.168.1.4", 4646);

    if(server->errorString() != ""){
        qWarning() << "Connection failed: " << server->errorString();
    }

    //Menu setup
    QMenu* menu = new QMenu("&Menu");
    menu->addAction("О Qt", this, SLOT(slotAboutQt()));
    menu->addSeparator();
    menu->addAction("Новая сетка", this, SLOT(createGrid()));
    menuBar->addMenu(menu);

    //Signal-slot setup
    connect(gridsView, SIGNAL(activated(QString)), this, SLOT(gridSelected(QString)));
    connect(play, SIGNAL(clicked(bool)), this, SLOT(playGrid()));
    connect(server, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(server, SIGNAL(readyRead()), this, SLOT(readyRead()));

    //Layout setup
    QVBoxLayout* mainLayout = new QVBoxLayout;
    QHBoxLayout* central = new QHBoxLayout;
    QVBoxLayout* leftPanel = new QVBoxLayout;
    mainLayout->addWidget(menuBar);
    leftPanel->addWidget(gridsView);
    leftPanel->addWidget(gridInfo);
    leftPanel->addWidget(play);
    central->addLayout(leftPanel);
    central->addWidget(lwg);
    central->addWidget(asw);
    mainLayout->addLayout(central);
    mainLayout->addWidget(player);
    setLayout(mainLayout);
    loadGrids();
}

MainWindow::~MainWindow()
{

}

void MainWindow::createGrid()
{
    GridCreationWindow* gcw = new GridCreationWindow;
    connect(gcw, SIGNAL(gridCreated(AudioGrid*)), this, SLOT(gridCreated(AudioGrid*)));
    gcw->show();
}

void MainWindow::gridCreated(AudioGrid *grid)
{
    grids.append(grid);
    connect(grid, SIGNAL(readyToPlay(AudioGrid*)), this, SLOT(gridTimerTimeout(AudioGrid*)));
    gridsView->addItem(grid->getName());
}

void MainWindow::itemClicked()
{

}

void MainWindow::playGrid()
{
    gridSelected(gridInfo->getName(), true);
}

void MainWindow::gridSelected(QString gridName, bool activate = false)
{
    for(auto grid : grids){
        if(grid->getName() == gridName){
            lwg->clear();
            for(auto file : grid->getFiles())
                lwg->addItem(file->name);
            gridInfo->setGrid(grid);
            if(activate)
                player->setList(grid->getFiles());
            break;
        }
    }
}

void MainWindow::gridSelected(QString gridName)
{
    for(auto grid : grids){
        if(grid->getName() == gridName){
            lwg->clear();
            for(auto file : grid->getFiles())
                lwg->addItem(file->name);
            gridInfo->setGrid(grid);
            break;
        }
    }
}

void MainWindow::gridTimerTimeout(AudioGrid *grid)
{
    player->setList(grid->getFiles());
    player->play_pause();
}

void MainWindow::slotAboutQt()
{
    QMessageBox::aboutQt(this, "О Qt");
}

void MainWindow::readyRead()
{
    server->blockSignals(true);
    QByteArray data = server->readAll();
    QString command = QString::fromUtf8(data);
    qDebug() << command;
    if(command == "GETLISTS"){
        QString out;
        for(auto grid : grids){
            out += grid->getName() + '\n';
        }
        server->write(out.toUtf8());
        server->waitForBytesWritten();
        server->flush();
        qDebug() << "Data sent";
    } else if (command.split(" ").first() == "play"){
        gridSelected(command.split(" ").last(), true);
    } else if (command == "stop"){
        player->stop();
    }
    server->readAll();
    server->blockSignals(false);
}
