#include <QApplication>

#include "mainwindow.h"
#include "global.h"

int main(int argc, char *argv[])
{
    app = new QApplication(argc, argv);
    MainWindow w;
    w.resize(800, 500);
    w.show();
    return app->exec();
}
