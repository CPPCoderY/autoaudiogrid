#ifndef GRIDSHOWWIDGET_H
#define GRIDSHOWWIDGET_H

#include <QtWidgets>
#include <QTimer>
#include <audiogrid.h>

class GridShowWidget : public QWidget
{
    Q_OBJECT
private:
    QLabel* lGrid;
    QLabel* lDuration;
    QLabel* lTimer;
    AudioGrid* grid = Q_NULLPTR;
public:
    GridShowWidget(QWidget *parent = 0);
    void setGrid(AudioGrid* newGrid);
    QString getName();
};

#endif // GRIDSHOWWIDGET_H
