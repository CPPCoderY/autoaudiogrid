#include "arduinoserialwidget.h"

QFrame *ArduinoSerialWidget::generateLine()
{
    QFrame* line = new QFrame();
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);
    return line;
}

void ArduinoSerialWidget::getTimeCorrection()
{
    if(serial->isWritable()){
        writeString("gettime&1");
        long  t = readString().toLong();
        sec_time_correction = (QTime::currentTime().msecsSinceStartOfDay() / 1000)  -  t;
        qDebug() << "Correction:" << sec_time_correction << "RTC:" << t << "PC:" << QDateTime::currentSecsSinceEpoch();
    }
}

QString ArduinoSerialWidget::readString()
{
    serial->waitForReadyRead();
    QString request(serial->readAll());
    return request;
}

void ArduinoSerialWidget::writeString(QString data)
{
    serial->write(data.toLocal8Bit());
}

ArduinoSerialWidget::ArduinoSerialWidget() : QWidget()
{
    setMinimumSize(270, 350);
    schoolTime = new QLCDNumber(this);
    realTime = new QLCDNumber(this);
    amplifier = new QPushButton("Включить усилитель");
    serial = new QSerialPort(this);
    deviceName = new QLabel("Устройство не подключено", this);
    connectionTimer = new QTimer(this);
    timeUpdateTimer = new QTimer(this);
    dte = new QTimeEdit;
    b_send_time = new QPushButton("Установить время");

    dte->setTime(QTime::currentTime());

    serial->setTextModeEnabled(true);

    //Timers setup
    connectionTimer->start(1000);
    timeUpdateTimer->start(1000);

    connect(connectionTimer, SIGNAL(timeout()), this, SLOT(tryConnect()));
    connect(timeUpdateTimer, SIGNAL(timeout()), this, SLOT(updateTime()));
    connect(b_send_time, SIGNAL(clicked(bool)), this, SLOT(timeSendBPressed()));
    connect(amplifier, SIGNAL(clicked(bool)), this, SLOT(amplifierBPressed()));

    realTime->setDigitCount(8);
    schoolTime->setDigitCount(8);
    schoolTime->setSegmentStyle(QLCDNumber::Filled);
    realTime->setSegmentStyle(QLCDNumber::Filled);

    //Layout setup
    QVBoxLayout* mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(deviceName);
    mainLayout->addWidget(generateLine());
    mainLayout->addWidget(new QLabel("Время в школе"));
    mainLayout->addWidget(schoolTime);
    mainLayout->addWidget(generateLine());
    mainLayout->addWidget(new QLabel("Московское время"));
    mainLayout->addWidget(realTime);
    mainLayout->addWidget(generateLine());
    mainLayout->addWidget(amplifier);
    mainLayout->addWidget(generateLine());
    mainLayout->addWidget(new QLabel("Установка времени звонка"));
    mainLayout->addWidget(dte);
    mainLayout->addWidget(b_send_time);
}

void ArduinoSerialWidget::setAmplifierEnabled(bool a_enabled)
{
    if(isConnected)
    {
        amplifierStatus = a_enabled;
        a_enabled ? writeString(QString("setrelay&1")) : writeString(QString("setrelay&0"));
        a_enabled ? amplifier->setText("Выключить усилитель") : amplifier->setText("Включить усилитель");
    } else {
        qWarning() << "Внешнее устройство не подключено!";
    }
}

void ArduinoSerialWidget::amplifierBPressed()
{
    setAmplifierEnabled(!amplifierStatus);
}

void ArduinoSerialWidget::timeSendBPressed()
{
    writeString(QString("settime&") + QString::number(dte->time().msecsSinceStartOfDay() / 1000));
    qInfo() << readString();
}

void ArduinoSerialWidget::tryConnect()
{
    if(!QSerialPortInfo().availablePorts().isEmpty()){
        connectionTimer->stop();
        serial = new QSerialPort(this);
        serial->setPort(QSerialPortInfo().availablePorts().first());
        serial->setBaudRate(9600);
        if(serial->open(QIODevice::ReadWrite)){
            qInfo() << "Школьный звонок подключен";
            connect(serial, SIGNAL(errorOccurred(QSerialPort::SerialPortError)),
                    this, SLOT(serialError(QSerialPort::SerialPortError)));
            QString cr = readString();
            qDebug() << cr;
            if(cr == "connectionrequest"){
                deviceName->setText("Запрос соединения");
                writeString("connectioninstall&1");
                deviceName->setText(QString("Устройство: ") + readString());
                isConnected = true;
                getTimeCorrection();
                setAmplifierEnabled(false);
            } else {
                qWarning() << "Что-то пошло не так, как планировалось";
                deviceName->setText(QString("Ошибка"));
            }
        } else {
            qWarning() << "Что вы вообще в комп воткнули?! Все плохо... Перезапускайте прогу";
        }
    }
}

void ArduinoSerialWidget::updateTime(){
    realTime->display(QTime::currentTime().toString("hh:mm:ss"));
    schoolTime->display(
                QDateTime::fromSecsSinceEpoch(
                    QDateTime::currentDateTime()
                    .toSecsSinceEpoch() - sec_time_correction).time().toString("hh:mm:ss"));
}

void ArduinoSerialWidget::serialError(QSerialPort::SerialPortError error)
{
    qWarning() << "Вы что-то там не то выдернули... Зачем? Короче ошибка №" << error;
    connectionTimer->start(500);
    sec_time_correction = 0;
    isConnected = false;
}
