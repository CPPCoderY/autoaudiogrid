#ifndef GRIDCREATIONWINDOW_H
#define GRIDCREATIONWINDOW_H

#include <QtWidgets>

#include <audiogrid.h>

class GridCreationWindow : public QWidget
{
    Q_OBJECT
private:
    QTreeView* files;
    QListWidget* newGridFiles;
    QFileSystemModel* fs;
    QStringList selectedItemsPaths;
    QPushButton* move;
    QPushButton* ok;
    QPushButton* cancel;
    QDateTimeEdit* dte;
    QLineEdit* nameEdit;

    AudioGrid* grid;

public:
    GridCreationWindow(QWidget *parent = 0);
signals:
    void gridCreated(AudioGrid*);
private slots:
    void moveClicked();
    void cancelClicked();
    void okClicked();
};

#endif // GRIDCREATIONWINDOW_H
