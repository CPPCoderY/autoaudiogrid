#include "gridshowwidget.h"

GridShowWidget::GridShowWidget(QWidget *parent) : QWidget(parent)
{
    lGrid  = new QLabel("Название сетки: ");
    lDuration = new QLabel("Длительность: ");
    lTimer = new QLabel("Воспроизведение: ");
    QVBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->addWidget(lGrid);
    mainLayout->addWidget(lDuration);
    mainLayout->addWidget(lTimer);
    setLayout(mainLayout);
    setMinimumSize(270, 350);
}

void GridShowWidget::setGrid(AudioGrid *newGrid)
{
    lGrid->setText(QString("Название сетки: %0").arg(newGrid->getName()));
    lDuration->setText(QString("Длительность: %0").arg(newGrid->getDuration()));
    lTimer->setText(QString("Воспроизведение: %0").arg(QDateTime::fromSecsSinceEpoch(
                                                           newGrid->getTimer()->getTime()).toString("hh:mm:ss")));
    grid = newGrid;
}

QString GridShowWidget::getName() {
    if(grid != Q_NULLPTR)
        return grid->getName();
    else
        return QString();
}
