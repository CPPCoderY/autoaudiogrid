#ifndef GLOBAL_H
#define GLOBAL_H

#include <QString>
#include <QListIterator>
#include <QApplication>

struct MediaFile;

typedef QListIterator<MediaFile*> MediaIter;

static QApplication* app;

static QString msecToString(quint64 msec)
{
    int hours = (msec / (60 * 60 * 1000));
    int minutes = ((msec % (60 * 60 * 1000)) / (60 * 1000));
    int seconds = ((msec % (60 * 1000) / 1000));
    int rMsec = 0;
    if(seconds != 0)
         rMsec= msec % seconds;
    return QString("%0:%1:%2").arg(hours).arg(minutes).arg(seconds);
}

struct MediaFile{
    QString name;
    QString path;
    int s_duration;
};

static long long sec_time_correction = 0;

#endif // GLOBAL_H
