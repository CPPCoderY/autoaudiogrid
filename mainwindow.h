#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets>
#include <QDir>
#include <QList>
#include <QTcpSocket>

#include <audioplayer.h>
#include <audiogrid.h>
#include <gridshowwidget.h>
#include <gridcreationwindow.h>
#include <arduinoserialwidget.h>
#include <gridcreationwindow.h>

class MainWindow : public QWidget
{
    Q_OBJECT
private:
    QList<AudioGrid*> grids;
    QMenuBar *menuBar;
    QComboBox* gridsView;
    ArduinoSerialWidget* asw;
    GridShowWidget* gridInfo;
    QListWidget *lwg;
    QLabel* currentName;
    QLabel* timerName;
    QPushButton* play;
    AudioPlayer *player;
    QTcpSocket* server;

    void loadGrids();
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void createGrid();
    void gridCreated(AudioGrid* grid);
    void itemClicked();
    void playGrid();
    void gridSelected(QString gridName, bool activate);
    void gridSelected(QString gridName);
    void gridTimerTimeout(AudioGrid*grid);
    void slotAboutQt();
    void readyRead();
};

#endif // MAINWINDOW_H
