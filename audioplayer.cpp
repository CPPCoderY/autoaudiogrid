#include "audioplayer.h"


void AudioPlayer::checkButtons()
{
    b_next->setEnabled(filesIterator->hasNext());
    b_prev->setEnabled(filesIterator->hasPrevious());
}

void AudioPlayer::updateStateLabel()
{
    switch(state){
    case Stop:
        stateLabel->setText("Остановлен");
        break;
    case Play:
        stateLabel->setText(QString("Играет: %0").arg(currentMedia->name));
        break;
    case Pause:
        stateLabel->setText(QString("Пауза: %0").arg(currentMedia->name));
        break;
    }
}

void AudioPlayer::playMedia()
{
    handControl(true);
    timelane->setMaximum(currentMedia->s_duration);
    timelane->setValue(0);
    player->setMedia(QUrl::fromLocalFile(currentMedia->path));
    state = Play;
    player->play();
    updateStateLabel();
    b_play->setIcon(QIcon(":/icons/pause.png"));
}

//void AudioPlayer::setupButtons()
//{
//    if(filesIterator != Q_NULLPTR){
//        if(filesIterator->hasPrevious()){
//            filesIterator->previous();
//            if(filesIterator->hasPrevious()){
//                b_prev->setEnabled(true);
//                prev_item_name->setText(filesIterator->peekPrevious()->name);
//            }  else {
//                b_prev->setEnabled(false);
//                prev_item_name->setText(NONE);
//            }
//            filesIterator->next();
//        }
//        if(filesIterator->hasNext()){
//            b_next->setEnabled(true);
//            next_item_name->setText(filesIterator->peekNext()->name);
//        }else {
//            b_next->setEnabled(false);
//            next_item_name->setText(NONE);
//            filesIterator->toBack();
//        }
//    } else {
//        handControl(false);
//    }
//}


AudioPlayer::AudioPlayer(QWidget *parent) : QWidget(parent)
{ 
    setMinimumSize(800, 100);
    player = new QMediaPlayer;
    b_play = new QPushButton;
    b_next = new QPushButton;
    b_prev = new QPushButton;
    timelane = new QSlider(Qt::Orientation::Horizontal);

    full_length = new QLCDNumber;
    current_pos = new QLCDNumber;
//    prev_item_name = new QLabel;
//    next_item_name = new QLabel;
    stateLabel = new QLabel;
    player = new QMediaPlayer(this);

    full_length->setSegmentStyle(QLCDNumber::Flat);
    current_pos->setSegmentStyle(QLCDNumber::Flat);

    full_length->setDigitCount(8);
    current_pos->setDigitCount(8);

    //Icons settings
    b_play->setIcon(QIcon(":/icons/play.png"));
    b_prev->setIcon(QIcon(":/icons/prev.png"));
    b_next->setIcon(QIcon(":/icons/next.png"));
    b_play->setIconSize(QSize(ICON_SIZE, ICON_SIZE));
    b_prev->setIconSize(QSize(ICON_SIZE, ICON_SIZE));
    b_next->setIconSize(QSize(ICON_SIZE, ICON_SIZE));

    //Size policy settings
    timelane->setSizePolicy(QSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Fixed));
//    prev_item_name->setSizePolicy(QSizePolicy(QSizePolicy::Policy::Minimum, QSizePolicy::Policy::Minimum));
//    next_item_name->setSizePolicy(QSizePolicy(QSizePolicy::Policy::Minimum, QSizePolicy::Policy::Minimum));
    stateLabel->setSizePolicy(QSizePolicy(QSizePolicy::Policy::Minimum, QSizePolicy::Policy::Minimum));

    stateLabel->setAlignment(Qt::AlignCenter);

    //Signals-slots setup
    connect(b_play, SIGNAL(clicked(bool)), this, SLOT(play_pause()));
    connect(b_next, SIGNAL(clicked(bool)), this, SLOT(next()));
    connect(b_prev, SIGNAL(clicked(bool)), this, SLOT(prev()));
    connect(timelane, SIGNAL(sliderMoved(int)), this, SLOT(sliderMoved(int)));
    connect(player, SIGNAL(positionChanged(qint64)),  this, SLOT(posChanged(qint64)));
    connect(player, SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)),
            this, SLOT(statusChanged(QMediaPlayer::MediaStatus)));
    connect(player, SIGNAL(durationChanged(qint64)), this, SLOT(durationChanged(qint64)));
    connect(timelane, SIGNAL(sliderPressed()), player, SLOT(pause()));
    connect(timelane, SIGNAL(sliderReleased()), player, SLOT(play()));

    //Layout setup
    QHBoxLayout* tl_bar = new QHBoxLayout();
    tl_bar->addWidget(current_pos);
    tl_bar->addWidget(timelane);
    tl_bar->addWidget(full_length);


    QHBoxLayout* controls = new QHBoxLayout();
//    controls->addWidget(prev_item_name);
    controls->addWidget(b_prev,  Qt::AlignHCenter);
    controls->addWidget(b_play,  Qt::AlignHCenter);
    controls->addWidget(b_next,  Qt::AlignHCenter);
//    controls->addWidget(next_item_name);

    QVBoxLayout* main = new QVBoxLayout(this);
    main->addWidget(stateLabel);
    main->addLayout(tl_bar);
    main->addLayout(controls);
    setLayout(main);

    handControl(false);

    state = Stop;

    updateStateLabel();
}

void AudioPlayer::setList(QList<MediaFile *> files)
{
    handControl(true);
    stop();
    if(filesIterator != Q_NULLPTR)
        delete filesIterator;
    filesToPlay = files;
    filesIterator = new QListIterator<MediaFile*>(files);
    if(filesIterator->hasNext()){
        currentMedia = filesIterator->next();
        updateStateLabel();
        playMedia();
//        setupButtons();
    } else {
        qWarning() << "Список воспроизведения пуст!";
    }
}


void AudioPlayer::handControl(bool hc)
{
    timelane->setEnabled(hc);
    b_play->setEnabled(hc);
    b_prev->setEnabled(hc);
    b_next->setEnabled(hc);
}

void AudioPlayer::play_pause() {
    if(state == Play){
        state = Pause;
        player->pause();
        b_play->setIcon(QIcon(":/icons/play.png"));
    } else if(state == Pause){
        state = Play;
        player->play();
        b_play->setIcon(QIcon(":/icons/pause.png"));
    }
}

void AudioPlayer::stop()
{
    state = Stop;
    player->stop();
    currentMedia = Q_NULLPTR;
    full_length->display("00:00:00");
    current_pos->display("00:00:00");
    updateStateLabel();
    timelane->setValue(0);
}

void AudioPlayer::next() {
    if(filesIterator->hasNext()){
        currentMedia = filesIterator->next();
        playMedia();
        checkButtons();
//       setupButtons();
    }
}

void AudioPlayer::prev() {
    if(filesIterator->hasPrevious()){
        currentMedia = filesIterator->previous();
        playMedia();
        checkButtons();
//        setupButtons();
    }
}

void AudioPlayer::durationChanged(qint64 duration)
{
    timelane->setRange(0, duration);
}

void AudioPlayer::statusChanged(QMediaPlayer::MediaStatus status)
{
    if(status == QMediaPlayer::MediaStatus::EndOfMedia){
        next();
    }
}

void AudioPlayer::posChanged(qint64 pos)
{
    timelane->setValue(pos);
    QString cp = msecToString(pos);
    QString fp = msecToString(player->duration());
    current_pos->display(cp);
    full_length->display(fp);
}

void AudioPlayer::sliderMoved(int pos)
{
    player->pause();
    player->setPosition(pos);
    current_pos->display(msecToString(pos));
    player->play();
}
