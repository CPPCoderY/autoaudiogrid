QT += core gui multimedia network
QT += widgets xml serialport

CONFIG += с++14

TARGET = AutoAudioGrid
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    audioplayer.cpp \
    audiogrid.cpp \
    gridcreationwindow.cpp \
    gridshowwidget.cpp \
    arduinoserialwidget.cpp

HEADERS  += mainwindow.h \
    audioplayer.h \
    audiogrid.h \
    gridcreationwindow.h \
    global.h \
    gridshowwidget.h \
    arduinoserialwidget.h

FORMS +=

RESOURCES += \
    resources.qrc

unix:!macx: LIBS += -L$$PWD/../../../../usr/lib/ -lbass

INCLUDEPATH += $$PWD/../../../../usr/include
DEPENDPATH += $$PWD/../../../../usr/include
