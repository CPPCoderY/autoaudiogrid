#ifndef AUDIOGRID_H
#define AUDIOGRID_H

#include <QList>
#include <QListIterator>
#include <QDateTime>
#include <QTimer>
#include <QFile>
#include <QDir>
#include <QDebug>
#include <QTextStream>
#include <QMediaPlayer>
#include <QMediaMetaData>
#include <QStringListModel>
#include <global.h>

class ModifiedTimer : public QObject{
    Q_OBJECT
private:
    quint64 activation_time;
    QTimer* timer;

public:
    ModifiedTimer(quint64 unixtime, int check_interval) {
        activation_time = unixtime;
        timer = new QTimer;
        timer->setInterval(check_interval);
        connect(timer, SIGNAL(timeout()), this, SLOT(check()));
    }

    void begin(){
        timer->start();
    }

    quint64 getTime(){
        return activation_time;
    }

    ~ModifiedTimer(){
        delete timer;
    }

private slots:
    void check(){
        if((QDateTime::currentDateTime().toSecsSinceEpoch() - sec_time_correction) == activation_time){
            timer->stop();
            emit grid_enabled();
        }
    }
signals:
    void grid_enabled();
};

class AudioGrid : public QObject
{
    Q_OBJECT
private:
    ModifiedTimer* _timer = Q_NULLPTR;
    QList<MediaFile*> _files;
    MediaIter* iterator;
    QString _name;
    int _position = -1;
    int _secDuration = 0;
    void calculateDuration();
public:
    AudioGrid(QString name, quint64 activation_time);
    ~AudioGrid();
    void addMediaFromFile(QString path);
    void saveToFile();
    MediaIter* getIterator() {return iterator;}
    static MediaFile* createMF(QString path);
    static AudioGrid* fromFile(QString path);
    QString getName() {return _name; }
    int getDuration() {return _secDuration; }
    QList<MediaFile *> getFiles() const;
    ModifiedTimer *getTimer() const;

public slots:
    void play();
signals:
    void readyToPlay(AudioGrid* grid);
};

#endif // AUDIOGRID_H
