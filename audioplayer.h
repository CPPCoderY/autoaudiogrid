#ifndef AUDIOPLAYER_H
#define AUDIOPLAYER_H

#include <QtWidgets>
#include <QMediaPlayer>
#include <QList>
#include <QStringList>
#include <QDir>
#include <QFile>

#include <audiogrid.h>
#include <global.h>

class AudioPlayer : public QWidget
{
    Q_OBJECT
private:
    QString NONE = "NaN";
    const int ICON_SIZE = 24;

    enum States{
        Play,
        Pause,
        Stop
    };

    QMediaPlayer* player;
    QPushButton *b_play;
    QPushButton *b_prev;
    QPushButton *b_next;
    QSlider* timelane;
    QLCDNumber* full_length;
    QLCDNumber* current_pos;
//    QLabel* prev_item_name;
    QLabel* stateLabel;
//    QLabel* next_item_name;

    AudioGrid* currentGrid = Q_NULLPTR;
    QList<MediaFile*> filesToPlay;
    QListIterator<MediaFile*> *filesIterator = Q_NULLPTR;

    MediaFile* currentMedia = Q_NULLPTR;

    int state = Pause;

    void checkButtons();
    void updateStateLabel();
    void playMedia();
//    void setupButtons();
    void handControl(bool hc);
public:
    AudioPlayer(QWidget *parent = 0);
    void setList(QList<MediaFile*> files);
signals:
    void listEnd();
public slots:
    void play_pause();
    void stop();
    void next();
    void prev();
    void durationChanged(qint64 duration);
    void statusChanged(QMediaPlayer::MediaStatus status);
    void posChanged(qint64 pos);
    void sliderMoved(int pos);
};

#endif // AUDIOPLAYER_H
