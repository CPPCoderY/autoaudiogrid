#include "gridcreationwindow.h"

GridCreationWindow::GridCreationWindow(QWidget *parent) : QWidget(parent)
{
    files = new QTreeView;
    newGridFiles = new QListWidget;
    fs = new QFileSystemModel;
    move = new QPushButton("Добавить файл ->");
    ok = new QPushButton("OK");
    cancel = new QPushButton("Отмена");
    dte = new QDateTimeEdit;
    nameEdit = new QLineEdit;

    dte->setDateTime(QDateTime::currentDateTime());
    nameEdit->setText("Имя сетки");

    fs->setRootPath(QDir::homePath());

    files->setModel(fs);

    //Signal-slot setup
    connect(move, SIGNAL(clicked(bool)), this, SLOT(moveClicked()));
    connect(ok, SIGNAL(clicked(bool)), this, SLOT(okClicked()));
    connect(cancel, SIGNAL(clicked(bool)), this, SLOT(cancelClicked()));

    //Layout setup
    QVBoxLayout* mainLayout = new QVBoxLayout;
    QHBoxLayout* top = new QHBoxLayout;
    top->addWidget(files);
    QVBoxLayout* central = new QVBoxLayout;
    central->addWidget(nameEdit);
    central->addWidget(move);
    central->addWidget(new QLabel("Дата авто-активации"));
    central->addWidget(dte);
    top->addLayout(central);
    top->addWidget(newGridFiles);
    QHBoxLayout* controls = new QHBoxLayout;
    controls->addWidget(cancel, Qt::AlignLeft);
    controls->addWidget(ok, Qt::AlignRight);
    mainLayout->addLayout(top);
    mainLayout->addLayout(controls);
    setLayout(mainLayout);
}

void GridCreationWindow::moveClicked()
{
    if(files->currentIndex().data() != 0){
        selectedItemsPaths.append(fs->filePath(files->currentIndex()));
    }
    newGridFiles->addItem(fs->fileName(files->currentIndex()));
}

void GridCreationWindow::cancelClicked()
{
    delete grid;
    close();
}

void GridCreationWindow::okClicked()
{
    grid = new AudioGrid(nameEdit->text(), dte->dateTime().toSecsSinceEpoch());
    for(auto path : selectedItemsPaths){
        grid->addMediaFromFile(path);
    }
    grid->saveToFile();
    emit gridCreated(grid);
    this->close();
}

